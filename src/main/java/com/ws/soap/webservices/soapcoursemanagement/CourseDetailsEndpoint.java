package com.ws.soap.webservices.soapcoursemanagement;
import com.bth.courses.GetCourseDetailsRequest;
import com.bth.courses.GetCourseDetailsResponse;


import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import org.springframework.beans.factory.annotation.Autowired;


import com.bth.courses.GetAllCourseDetailsRequest;
import com.bth.courses.GetAllCourseDetailsResponse;

import com.bth.courses.DeleteCourseDetailsRequest;
import com.bth.courses.DeleteCourseDetailsResponse;

//import com.bth.courses.ServiceStatus;

@Endpoint
public class CourseDetailsEndpoint {

    private CourseRepository courseRepository;

    @Autowired
    public CourseDetailsEndpoint(CourseRepository courseRepository){
        this.courseRepository = courseRepository;
    }

    @PayloadRoot(namespace="http://bth.com/courses", localPart = "GetCourseDetailsRequest")
    @ResponsePayload 
    public GetCourseDetailsResponse processCourseDetailsRequest(@RequestPayload GetCourseDetailsRequest request){
        

        
        GetCourseDetailsResponse response = new GetCourseDetailsResponse();

        /*CourseDetails courseDetails = new CourseDetails();
        courseDetails.setId(request.getId());
        courseDetails.setDescription("That would be wonderful course!");
        courseDetails.setName("spring");*/
        /*String s = Integer.toString(request.getId());
        BigInteger i = new BigInteger(s);*/
        response.setCourseDetails(courseRepository.findCourseDetails(request.getId()));

        return response;
    }


    @PayloadRoot(namespace="http://bth.com/courses", localPart = "GetAllCourseDetailsRequest")
    @ResponsePayload 
    public GetAllCourseDetailsResponse processAllCourseDetailsRequest(@RequestPayload GetAllCourseDetailsRequest request){
        

        
        GetAllCourseDetailsResponse response = new GetAllCourseDetailsResponse();
        response.setCourseDetails(courseRepository.findAllCoursesDetails());

        return response;
    }
    @PayloadRoot(namespace="http://bth.com/courses", localPart = "DeleteCourseDetailsRequest")
    @ResponsePayload 
    public DeleteCourseDetailsResponse processDeleteCourseDetailsRequest(@RequestPayload DeleteCourseDetailsRequest request){
        
        DeleteCourseDetailsResponse response = new DeleteCourseDetailsResponse();
        
        //String errorMessage = "ERROR";
        //ServiceStatus serviceStatus = new ServiceStatus();
        response.setServiceStatus(courseRepository.deteCourseDetails(request.getId()));
		//serviceStatus.setStatusCode("NOT_FOUND");
		//serviceStatus.setMessage("Course with id: " + request.getId() + " not found. Cannot delete Movie");

		//throw new ServiceFaultException(serviceStatus.getMessage(), serviceStatus);

        return response;
    }
}

//./mvnw spring-boot:run
