package com.ws.soap.webservices.soapcoursemanagement;


import java.util.HashMap;


import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import com.bth.courses.CourseDetails;
import java.math.BigInteger;

import com.bth.courses.AllCourseDetails;
import com.bth.courses.ServiceStatus;


@Component
public class CourseRepository {
    public enum Status {
		SUCCESS, FAILURE;
	}
    private final Map<BigInteger, CourseDetails> courses = new HashMap<>();

    private  final   AllCourseDetails allcoursdetails = new AllCourseDetails();
    //private static List coursess = new ArrayList<>();

    @PostConstruct
    public void initData(){

        CourseDetails course = new CourseDetails();
        BigInteger i = new BigInteger("123");
        course.setId(i);
        course.setName("Spring");
        course.setDescription("That would be wonderful course!");
        courses.put(course.getId(),course);
        //coursess.add(course);

        allcoursdetails.getCourseDetails().add(course);

        course = new CourseDetails();
        i = new BigInteger("124");
        course.setId(i);
        course.setName("Webservice");
        course.setDescription("That would be wonderful course!");
        courses.put(course.getId(),course);
        //coursess.add(course);
        allcoursdetails.getCourseDetails().add(course);

        course = new CourseDetails();
        i = new BigInteger("125");
        course.setId(i);
        course.setName("SOAP");
        course.setDescription("That would be wonderful course!");  
        courses.put(course.getId(),course);
        //coursess.add(course);

        allcoursdetails.getCourseDetails().add(course);

        course = new CourseDetails();
        i = new BigInteger("126");
        course.setId(i);
        course.setName("IHM");
        course.setDescription("That would be create a responsive application!");
        courses.put(course.getId(),course);
        //coursess.add(course);

        allcoursdetails.getCourseDetails().add(course);
        
    }
    /*static{
        CourseDetails course = new CourseDetails();
        BigInteger i = new BigInteger("123");
        course.setId(i);
        course.setName("Spring");
        course.setDescription("That would be wonderful course!");
        //courses.put(course.getId(),course);
        coursess.add(course);
        //allcoursdetails.getCourseDetails().add(course);

        course = new CourseDetails();
        i = new BigInteger("124");
        course.setId(i);
        course.setName("Webservice");
        course.setDescription("That would be wonderful course!");
        //courses.put(course.getId(),course);
        coursess.add(course);

        //allcoursdetails.getCourseDetails().add(course);

        course = new CourseDetails();
        i = new BigInteger("125");
        course.setId(i);
        course.setName("SOAP");
        course.setDescription("That would be wonderful course!");  
        //courses.put(course.getId(),course);
        coursess.add(course);

        //allcoursdetails.getCourseDetails().add(course);

        course = new CourseDetails();
        i = new BigInteger("126");
        course.setId(i);
        course.setName("IHM");
        course.setDescription("That would be create a responsive application!");
        //courses.put(course.getId(),course);
        coursess.add(course);

        //allcoursdetails.getCourseDetails().add(course);
    }*/

    public CourseDetails findCourseDetails(BigInteger id){
        if(this.courses.get(id)==null){
            Assert.notNull(null, "The Course's id must not be null");
            return null;
       }else{
            return this.courses.get(id);
       }
    }
    public AllCourseDetails findAllCoursesDetails(){
        
        if(this.allcoursdetails.getCourseDetails().isEmpty()){
            Assert.notNull(null,"Have not Courses");
            return null;
        }else{
            return this.allcoursdetails;
        }
        
        
    }
   /*public AllCourseDetails deteCourseDetails(BigInteger id){
       
        if(this.courses.get(id)==null){
            Assert.notNull(null,"The Courses's id must not be null");
            return null;
        }else{
            this.allcoursdetails.getCourseDetails().remove(this.courses.get(id));
            this.courses.remove(id);
            if(this.allcoursdetails.getCourseDetails().isEmpty()){
                Assert.notNull(null,"Have not Courses");
                return null;
            }else{
                return this.allcoursdetails;
            }          
            
        }
    }*/

    public ServiceStatus deteCourseDetails(BigInteger id) {
		//Iterator iterator = coursess.iterator();
        ServiceStatus status = new ServiceStatus();
        if(id==null){
            // return Status.FAILURE;
            status.setStatusCode("ERROR");
            status.setMessage("Please give an id this can not be empty");
            return status;
        }else if(this.courses.get(id)==null ){
			
			// return Status.FAILURE;
            status.setStatusCode("ERROR");
            status.setMessage("Course with id: " +id + " not found. Cannot be deleted");
            return status;
			
		}else{
		
            this.allcoursdetails.getCourseDetails().remove(this.courses.get(id));
            this.courses.remove(id);
			// return Status.SUCCESS;

			status.setStatusCode("OK");
			status.setMessage("Course with id: " + id+ " is now deleted");
			return status;
        }
	}


}
// ./mvnw spring-boot:run