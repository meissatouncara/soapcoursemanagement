package com.ws.soap.webservices.soapcoursemanagement;

//import java.util.Properties;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
//import org.springframework.ws.soap.server.endpoint.SoapFaultDefinition;
//import org.springframework.ws.soap.server.endpoint.SoapFaultMappingExceptionResolver;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;



// A personnaliser


@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
  @SuppressWarnings({ "rawtypes", "unchecked" })  
  @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext){
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }
  @Bean(name = "course-details")
  public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema coursedetailsSchema) {
    DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
    wsdl11Definition.setPortTypeName("CoursePort");
    wsdl11Definition.setLocationUri("/ws");
    wsdl11Definition.setTargetNamespace("http://bth.com/courses");
    wsdl11Definition.setSchema(coursedetailsSchema);
    return wsdl11Definition;
  }
  @Bean
  public XsdSchema coursedetailsSchema() {
    return new SimpleXsdSchema(new ClassPathResource("course-details.xsd"));
  }
  /*@Bean(name = "allCourse-details")
  public DefaultWsdl11Definition defaultWsdl11DefinitionAll(XsdSchema allCoursedetailsSchema) {
    DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
    wsdl11Definition.setPortTypeName("AllCoursePort");
    wsdl11Definition.setLocationUri("/ws");
    wsdl11Definition.setTargetNamespace("http://bth.com/courses");
    wsdl11Definition.setSchema(allCoursedetailsSchema);
    return wsdl11Definition;
  }
  @Bean
  public XsdSchema allCoursedetailsSchema() {
    return new SimpleXsdSchema(new ClassPathResource("allCourse-details.xsd"));
  }
  /*@Bean(name = "course-delete")
  public DefaultWsdl11Definition defaultWsdl11DefinitionDelete(XsdSchema deleteCoursedetailsSchema) {
    DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
    wsdl11Definition.setPortTypeName("DeleteCoursePort");
    wsdl11Definition.setLocationUri("/ws");
    wsdl11Definition.setTargetNamespace("http://bth.com/courses");
    wsdl11Definition.setSchema(deleteCoursedetailsSchema);
    return wsdl11Definition;
  }
  @Bean
  public XsdSchema deleteCoursedetailsSchema() {
    return new SimpleXsdSchema(new ClassPathResource("course-delete.xsd"));
  }*/

  // ajouter inspiration movies
 /* @Bean
	public SoapFaultMappingExceptionResolver exceptionResolver() {
		SoapFaultMappingExceptionResolver exceptionResolver = new DetailSoapFaultDefinitionExceptionResolver();

		SoapFaultDefinition faultDefinition = new SoapFaultDefinition();
		faultDefinition.setFaultCode(SoapFaultDefinition.SERVER);
		exceptionResolver.setDefaultFault(faultDefinition);

		Properties errorMappings = new Properties();
		errorMappings.setProperty(Exception.class.getName(), SoapFaultDefinition.SERVER.toString());
		errorMappings.setProperty(ServiceFaultException.class.getName(), SoapFaultDefinition.SERVER.toString());
		exceptionResolver.setExceptionMappings(errorMappings);
		exceptionResolver.setOrder(1);
		return exceptionResolver;
	}*/

}